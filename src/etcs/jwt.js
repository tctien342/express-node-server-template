import { Logger, resCall } from '.';
import { Manager } from '../manager';
import { ModelManager } from '../models';
import * as jwt from 'jsonwebtoken';

/**
 * Jwt handle class
 */
class JWTAuthorize {
	manager: Manager;
	logger: Logger;
	models: ModelManager;

	constructor(manager: Manager, models: ModelManager) {
		this.manager = manager;
		this.logger = new Logger('JWTAuthorize', manager);
		this.models = models;
	}

	/**
	 * Generate an jwt token base on id string
	 * @param {string} user_id id of object to generate
	 */
	generate_user(user_id) {
		let payload = {
			id: user_id,
			type: 'api',
			exp:
				Math.floor(Date.now() / 1000) +
				this.manager.configs.JWT.EXPIRE_TIME,
		};
		try {
			return jwt.sign(payload, this.manager.configs.JWT.SECRET_KEY);
		} catch (e) {
			this.logger.log('verify_token', e);
			return false;
		}
	}

	/**
	 * Generate an jwt refresh token base on id string
	 * @param {string} user_id id of object to generate
	 */
	generate_refresh_token(user_id) {
		let payload = {
			id: user_id,
			type: 'refresh',
			exp:
				Math.floor(Date.now() / 1000) +
				this.manager.configs.JWT.REFRESH_TOKEN_EXPIRE_TIME,
		};
		try {
			return jwt.sign(payload, this.manager.configs.JWT.SECRET_KEY);
		} catch (e) {
			this.logger.log('generate_refresh_token', e);
			return false;
		}
	}

	/**
	 * Verify an token
	 * @param {string} token token to be verified
	 * @param {resCall} callback an callback function when verify done, will return status, data and message
	 */
	verify_token(token, callback = () => {}) {
		try {
			let payload = jwt.verify(
				token,
				this.manager.configs.JWT.SECRET_KEY,
			);
			if (!payload.hasOwnProperty('type')) {
				callback(
					this.logger.result_maker(
						false,
						{},
						'Wrong token payload',
						'verify_token',
					),
				);
			} else {
				if (payload.type !== 'api') {
					callback(
						this.logger.result_maker(
							false,
							{},
							'This token can not be used for access data',
							'verify_token',
						),
					);
				} else {
					this.models.User.get_by_id(payload.id, state => {
						if (state.status) {
							callback(
								this.logger.result_maker(
									true,
									state.data,
									'Success verify token',
									'verify_token',
								),
							);
						} else {
							callback(
								this.logger.result_maker(
									false,
									state.data,
									state.message,
									'verify_token',
								),
							);
						}
					});
				}
			}
		} catch (e) {
			this.logger.log('verify_token', e);
			callback(
				this.logger.result_maker(
					false,
					e,
					'Can not verify token',
					'verify_token',
				),
			);
		}
	}

	/**
	 * Verify refresh token
	 * @param {string} refresh_token refresh token to be verified
	 * @param {resCall} callback an callback function when verify done, will return status, data and message
	 */
	refresh_token(refresh_token, callback = () => {}) {
		try {
			let payload = jwt.verify(
				refresh_token,
				this.manager.configs.JWT.SECRET_KEY,
			);
			if (!payload.hasOwnProperty('type')) {
				callback(
					this.logger.result_maker(
						false,
						{},
						'Wrong token payload',
						'refresh_token',
					),
				);
			} else {
				if (payload.type !== 'refresh') {
					callback(
						this.logger.result_maker(
							false,
							{},
							'This token can not be used for refresh access token',
							'refresh_token',
						),
					);
				} else {
					let token = {
						access: this.generate_user(payload.id),
						refresh: this.generate_refresh_token(payload.id),
					};
					callback(
						this.logger.result_maker(
							true,
							token,
							'Success refresh token',
							'refresh_token',
						),
					);
				}
			}
		} catch (e) {
			callback(
				this.logger.result_maker(
					false,
					e,
					'Can not verify refresh token',
					'refresh_token',
				),
			);
		}
	}
}

export { JWTAuthorize };
