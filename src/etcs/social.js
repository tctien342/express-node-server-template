import { Logger, resCall } from '.';
import * as graph from 'fbgraph';
import * as request from 'request';
import { Manager } from '../manager';

/**
 * Social login handler for server
 */
class Social {
	manager: Manager;
	logger: Logger;

	constructor(manager: Manager) {
		this.manager = manager;
		this.logger = new Logger('Social', manager);
	}

	/**
	 * Authorize with facebook and get user data
	 * @param {string} access_token access token from request
	 * @param {resCall} callback return user data on callback
	 */
	auth_facebook(access_token, callback = () => {}) {
		if (!access_token) {
			callback(
				this.logger.result_maker(
					false,
					{},
					'Access_token is null',
					'auth_facebook',
				),
			);
		}
		try {
			graph.get(
				'me',
				{ access_token: access_token, fields: 'name,gender' },
				(err, data) => {
					if (err) {
						callback(
							this.logger.result_maker(
								false,
								err,
								'Access token authorize failed',
								'auth_facebook',
							),
						);
						// this.logger.log('auth_facebook', err)
					} else {
						// Init avatar value
						data.avatar =
							'http://graph.facebook.com/' +
							data.id +
							'/picture?type=large';
						// Bind access token
						data.token = access_token;
						// Callback function
						callback(
							this.logger.result_maker(
								true,
								Social.make_user_facebook(data),
								'Login success',
								'auth_facebook',
							),
						);
					}
				},
			);
		} catch (e) {
			callback(
				this.logger.result_maker(
					false,
					e,
					'Can not call graph api',
					'auth_facebook',
				),
			);
		}
	}

	/**
	 * Authorize with google and get user data
	 * @param {string} access_token access token from request
	 * @param {resCall} callback return user data on callback
	 */
	auth_google(access_token, callback = () => {}) {
		if (!access_token) {
			callback(
				this.logger.result_maker(
					false,
					{},
					'Access_token is null',
					'auth_google',
				),
			);
		}
		let req = this.manager.configs.URL.GOOGLE_AUTHORIZE + access_token;
		try {
			request(req, (error, response, body) => {
				if (!error && response.statusCode === 200) {
					let received_data = JSON.parse(body);
					if (received_data.hasOwnProperty('kind')) {
						// Bind access token
						received_data.token = access_token;
						callback(
							this.logger.result_maker(
								true,
								Social.make_user_google(received_data),
								'Login success',
								'auth_google',
							),
						);
					} else {
						callback(
							this.logger.result_maker(
								false,
								{},
								'Authorize failed',
								'auth_google',
							),
						);
					}
				} else {
					callback(
						this.logger.result_maker(
							false,
							{},
							'Authorize failed',
							'auth_google',
						),
					);
				}
			});
		} catch (e) {
			// callback(false);
			// this.logger.log('auth_google', e)
			callback(
				this.logger.result_maker(
					false,
					e,
					'Can not call google api',
					'auth_google',
				),
			);
		}
	}

	/**
	 * Make return form data from facebook user info
	 * @param {object} object facebook user info as object
	 */
	static make_user_facebook(object) {
		return {
			tp_id: object.id,
			tp_token: object.token,
			tp_type: 'facebook',
			name: object.name,
			gender: object.gender === 'male' ? 0 : 1,
			avatar: object.avatar,
		};
	}

	/**
	 * Make return form data from google user info
	 * @param {object} object google user info as object
	 */
	static make_user_google(object) {
		return {
			tp_id: object.id,
			tp_token: object.token,
			tp_type: 'google',
			name: object.displayName,
			gender: object.gender === 'male' ? 0 : 1,
			avatar: object.image.url + '0',
		};
	}
}

export { Social };
