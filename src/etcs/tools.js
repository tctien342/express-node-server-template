/**
 * Delete object keys that present in array key
 * @param {object} obj object need to be delete keys
 * @param {array} arr_key array key of object need to delete
 */
function delete_field(obj: Object = {}, arr_key: Array = []) {
	arr_key.forEach(ele => {
		obj[ele] = undefined;
	});
	return obj;
}

/**
 * Clone an object with all key present in array
 * @param {object} obj object need to be cloned
 * @param {array} arr_key includes keys that you want to clone from object
 */
function clone_obj_by_keys(obj: Object = {}, arr_key: Array = []) {
	let new_obj = {};
	arr_key.forEach(key => {
		new_obj[key] = obj[key];
	});
	return new_obj;
}

/**
 * Verify an object that had only key present in array
 * @param {object} obj object need to be verified
 * @param {array} arr_key keys that need to be verified
 */
function verify_obj_by_keys(obj: Object = {}, arr_key: Array = []) {
	let obj_keys = Object.keys(obj);
	let check = true;
	obj_keys.forEach(key => {
		if (!arr_key.includes(key)) check = false;
	});
	return check;
}

/**
 * Verify an object that had enough key present in array
 * @param {object} obj object need to be verified
 * @param {array} arr_key keys that need to be verified
 */
function verify_obj_enough_keys(obj: Object = {}, arr_key: Array = []) {
	let obj_keys = Object.keys(obj);
	arr_key.forEach(key => {
		if (!obj_keys.includes(key)) {
			return false;
		}
	});
	return true;
}

/**
 * Update target object's value from source object's value
 * @param {object} tar_obj object need to be update value
 * @param {object} src_obj object that all value in it will update to target object
 */
function update_obj_from_obj(tar_obj: Object = {}, src_obj: Object = {}) {
	let src_keys = Object.keys(src_obj);
	src_keys.forEach(key => {
		tar_obj[key] = src_obj[key];
	});
	return tar_obj;
}

/**
 * Return an object order by it return times
 * @param {array} array_obj array object which had count attribute on each object
 * @param {number} threshold threshold of each time
 * @param {string} show_key key that store count return times
 */
function get_object_by_show_count_percent(
	array_obj: Array = [],
	threshold: Number = 10,
	show_key: String = 'show_count',
) {
	// Clone to process array
	let process_arr = [];
	let share_sum = 0;
	let equal_percent_foreach = 100.0 / array_obj.length;
	let min_show = array_obj[0][show_key];
	for (let i = 0; i < array_obj.length; i++) {
		let new_obj = {};
		new_obj.index = i;
		new_obj.percent = 0;
		process_arr.push(new_obj);
		if (array_obj[i][show_key] < min_show)
			min_show = array_obj[i][show_key];
	}

	// Calculate percent for items
	for (let i = 0; i < process_arr.length; i++) {
		process_arr[i].percent =
			equal_percent_foreach /
			(Math.pow(array_obj[i][show_key] - min_show, threshold) + 1);
		let share_percent = equal_percent_foreach - process_arr[i].percent;
		if (share_percent > 0) {
			share_sum += share_percent;
		}
	}

	// Generate a random value base on left percent
	let random_result = Math.random() * (100.0 - share_sum);

	// Getting return item
	let found_index = 0;
	let percent_index = process_arr[found_index].percent;
	while (percent_index < random_result) {
		if (found_index === process_arr.length - 1) {
			break;
		}
		found_index++;
		percent_index += process_arr[found_index].percent;
	}
	return array_obj[found_index];
}

/**
 * Sort random an array
 * @param {array} arr array that need to be sort
 */
function random_sort_array(arr: Array = []) {
	arr.sort(function() {
		return 0.5 - Math.random();
	});
	return arr;
}

// return true if target obj is different from source obj

/**
 * Compare to object if it is the same
 * @param {object} obj_tar object need to be verified
 * @param {object} obj_src object will be compare
 * @param {array} expect keys that don't need to compare
 */
function check_different_object(
	obj_tar: Object = {},
	obj_src: Object = {},
	expect: Array = [],
) {
	let keys = Object.keys(obj_src);
	let check = false;
	keys.forEach(key => {
		if (expect.includes(key)) return;
		if (obj_src[key] !== obj_tar[key]) {
			check = true;
		}
	});
	return check;
}
/**
 * Random an string base on length
 * @param {number} strLength length of the string that will return
 */
function ranString(strLength: Number = 4) {
	var text = '';
	var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';

	for (var i = 0; i < strLength; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

export {
	delete_field,
	verify_obj_by_keys,
	verify_obj_enough_keys,
	clone_obj_by_keys,
	update_obj_from_obj,
	get_object_by_show_count_percent,
	random_sort_array,
	check_different_object,
	ranString,
};
