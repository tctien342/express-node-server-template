type resObject = { status: Number, data: Object, message: String };

const resCall = (res: resObject) => {};

/**
 * List of status code for response api
 */
const CODE = {
	/**
	 * Success processing
	 */
	SUCCESS: 0,
	/**
	 * Token error
	 */
	TOKEN_ERR: 1,
	/**
	 * Social authorization error
	 */
	TP_ERR: 2,
	/**
	 * Internal server error
	 */
	SERVER_ERR: 3,
	/**
	 * Request's input not correct
	 */
	INPUT_ERR: 4,
	/**
	 * Request permission error
	 */
	PERMISSION_ERR: 5,
	/**
	 * Hack detected (API flow not correct)
	 */
	HACK_ERR: 7,
	/**
	 * Not found error, data?
	 */
	NOTFOUND_ERR: 8,
	/**
	 * Version of client not meet
	 */
	VERSION_ERR: 9,
};

/**
 * Make an form response
 * @param {object} data data to create response
 * @param {number} code status of response
 * @param {string} message note for response
 */
const response_maker = (
	data: Object,
	code: Number = CODE.SUCCESS,
	message: String = 'None',
) => {
	return JSON.stringify({ status: code, data: data, message: message });
};

const Responder = {
	CODE: CODE,
	make: response_maker,
};

export { Responder, resCall };
