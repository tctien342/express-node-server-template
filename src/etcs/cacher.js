import { Logger } from '.';
import { Manager } from '../manager';

class Cacher {
	/**
	 * Name of cache object
	 */
	name: String;
	manager: Manager;
	logger: Logger;
	/**
	 * Cache store object
	 */
	cache: Object;
	dynamic_cache: Number;
	/**
	 * dynamic_multi explain:
	 *		The default time checking to clean cache is 30 minute equal to 1800s
	 *		if users lower than 500 as default so checking time not change
	 *		but if more than 500, the checking time is divide to 2 equal to 15 minute and
	 *		more than 1000 is 10 minute and so on ...
	 *		Max default is 5 equal 6 minute frequency
	 */
	dynamic_multi: Number;

	constructor(name, manager: Manager) {
		this.name = name;
		this.manager = manager;
		this.logger = new Logger(name.toUpperCase() + ' CACHER', manager);
		this.cache = {};
		this.dynamic_cache = 0;
		this.dynamic_multi = 1;
		if (
			this.manager.configs.CACHER.ACTIVE &&
			this.manager.configs.CACHER.CLEANER
		) {
			setTimeout(() => {
				this.data_helper();
			}, this.manager.configs.CACHER.INTERVAL);
		}
	}

	/**
	 * Data helper is a interval function that help renew data in circle time
	 */
	data_helper() {
		this.logger.log('data_helper', 'Cleaning...');
		let users = Object.keys(this.cache);
		if (this.manager.configs.CACHER.DYNAMIC) {
			// More user come -> more user out -> better ram manager
			this.dynamic_cache =
				users.length * this.manager.configs.CACHER.DYNAMIC_CACHE;
			// More user -> more checking in time
			this.dynamic_multi =
				parseInt(
					users.length /
						this.manager.configs.CACHER.DYNAMIC_FREQ_CHECK_USER,
					36,
				) + 1;
			if (
				this.dynamic_multi >
				this.manager.configs.CACHER.MAX_DYNAMIC_MULTI
			)
				this.dynamic_multi = this.manager.configs.CACHER.MAX_DYNAMIC_MULTI;
		}
		// Checking time cache
		users.forEach(user => {
			let functions = Object.keys(this.cache[user]);
			functions.forEach(funct => {
				if (this.cache[user][funct]) {
					if (
						Date.now() -
							this.cache[user][funct].time +
							(this.manager.configs.CACHER.DYNAMIC
								? this.dynamic_cache
								: 0) >
						this.manager.configs.CACHER.REFRESH_TIME
					) {
						this.logger.log('data_helper', 'Empty..' + user);
						this.cache[user][funct] = undefined;
					}
				}
			});
		});

		// Setting to call in new time
		setTimeout(() => {
			this.data_helper();
		}, this.manager.configs.CACHER.INTERVAL / (this.manager.configs.CACHER.DYNAMIC ? this.dynamic_multi : 1));
	}

	/**
	 * Add data to cache
	 * @param {*} user_id id of object
	 * @param {*} function_name name of function to store it return
	 * @param {*} data data to stores
	 */
	add(user_id, function_name, data) {
		if (!this.manager.configs.CACHER.ACTIVE) return false;
		if (!this.cache[user_id]) this.cache[user_id] = {};
		this.cache[user_id][function_name] = { data: data, time: Date.now() };
		this.logger.log('add', 'add cache ' + user_id + '_' + function_name);
		return true;
	}

	/**
	 * Delete data in cache
	 * @param {*} user_id id of object
	 * @param {*} function_name name of function stored
	 */
	del(user_id, function_name) {
		if (!this.manager.configs.CACHER.ACTIVE) return false;
		if (!this.cache[user_id]) return true;
		this.cache[user_id][function_name] = undefined;
		this.logger.log('del', 'del cache ' + user_id + '_' + function_name);
		return true;
	}

	/**
	 * Get data from cache
	 * @param {*} user_id id of object
	 * @param {*} function_name name of function to get data
	 */
	get(user_id, function_name) {
		if (!this.manager.configs.CACHER.ACTIVE) return null;
		if (this.cache[user_id] && this.cache[user_id][function_name]) {
			this.logger.log('get', 'cached ' + user_id + '_' + function_name);
			this.cache[user_id][function_name].time = Date.now();
			return this.cache[user_id][function_name].data;
		} else {
			this.logger.log(
				'get',
				'not cached ' + user_id + '_' + function_name,
			);
			return null;
		}
	}

	/**
	 * When data user update, call this function to empty cache of user
	 * @param {*} user_id id of object to remove
	 */
	empty(user_id) {
		if (!this.manager.configs.CACHER.ACTIVE) return false;
		this.logger.log('empty cache ', user_id);
		this.cache[user_id] = undefined;
		return true;
	}

	/**
	 * Return all key in cache object
	 */
	keys() {
		return Object.keys(this.cache);
	}
}

export { Cacher };
