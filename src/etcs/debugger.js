import { Manager } from '../manager';

let chalk = require('chalk');

class Logger {
	name: String;
	manager: Manager;

	/**
	 * New logger class for object
	 * @param {String} name Name of object to be log
	 * @param {Object} manager Manager object
	 */
	constructor(name, manager: Manager, initLog: Boolean = true) {
		this.name = name;
		this.manager = manager;
		if (initLog)
			this.re_console(
				name.toUpperCase() + ' is ' + chalk.greenBright('activated'),
			);
	}

	/**
	 * Log to screen function
	 * @param {String} func Name of function to be logged
	 * @param {String} log Log to print
	 * @param {Number} success -1 if failed
	 */
	log(func = 'None', log = 'None', success = -1) {
		if (typeof log !== 'string') {
			this.re_console(
				chalk.yellowBright(this.name) +
					' > ' +
					func.toUpperCase() +
					' > ',
				success,
			);
			console.log(log);
		} else {
			this.re_console(
				chalk.yellowBright(this.name) +
					' > ' +
					func.toUpperCase() +
					' > ' +
					log.toUpperCase(),
				success,
			);
		}
	}

	/**
	 * Print to console function
	 * @param {String} text String output to console
	 * @param {Number} success -1 if failed
	 */
	re_console(text = 'None', success = -1) {
		if (success !== -1) {
			if (success) {
				if (this.manager.configs.SERVER.DEBUG_LEVEL > 1)
					console.log(chalk.green('DONE'), '> ' + text);
			} else {
				if (this.manager.configs.SERVER.DEBUG_LEVEL > 0)
					console.log(chalk.red('FAILED'), '> ' + text);
			}
		} else {
			if (this.manager.configs.SERVER.DEBUG_LEVEL > 2)
				console.log(chalk.cyan('LOGGER'), '> ' + text);
		}
	}

	/**
	 * Create an result then log it to screen
	 * @param {Bool} success True if success
	 * @param {Object} data Data to be logged to console and send to result
	 * @param {String} message Message of this log
	 * @param {String} func Name of function which call this log
	 */
	result_maker(success = true, data = {}, message = '', func = 'None') {
		this.log(func, message, success);
		return {
			status: success,
			data: data,
			message: message,
		};
	}
}

export { Logger };
