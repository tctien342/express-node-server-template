import { AppConfig } from './configs';
import { Logger } from './etcs';
import * as fs from 'fs';
import * as chalk from 'chalk';

class Manager {
	logger: Logger;
	memory_cleaner: Function;

	// init default variable
	configs = AppConfig;
	server_status = {
		OFFLINE: 0,
		NORMAL: 1,
		MAINTAIN: 2,
	};

	constructor() {
		this.logger = new Logger('AppConfig', this, false);
		this.show_server_status();
		if (this.configs.MEMORY.CLEANER) {
			this.memory_cleaner = setInterval(
				this.heap_cleaner,
				this.configs.MEMORY.INTERVAL,
			);
		}
	}

	/**
	 * Optimize ram task
	 */
	heap_cleaner() {
		try {
			global.gc();
		} catch (e) {
			console.log(
				chalk.default.yellowBright('WARNING > ') +
					"You must run program with 'node --expose-gc index.js' or 'npm start'",
			);
			console.log(
				chalk.default.cyanBright('SUCCESS > ') + 'DISABLED RAM CLEANER',
			);
			clearInterval(this.memory_cleaner);
		}
	}

	/**
	 * Init server status
	 */
	show_server_status() {
		console.log('***************************************');
		console.log(
			'SERVER NAME    : ' +
				chalk.default.cyanBright(this.configs.APP_NAME.toUpperCase()),
		);
		console.log(
			'SERVER STATUS  : ' +
				chalk.default.cyanBright(
					this.configs.SERVER.STATUS === this.server_status.NORMAL
						? 'NORMAL'
						: this.configs.SERVER.STATUS ===
						  this.server_status.OFFLINE
						? 'OFFLINE'
						: 'MAINTAIN',
				),
		);
		console.log(
			'SERVER VERSION : ' +
				chalk.default.cyanBright(this.configs.SERVER.VERSION),
		);
		console.log(
			'DEBUG LEVEL    : ' +
				this.get_debug_log(this.configs.SERVER.DEBUG_LEVEL),
		);
		console.log(
			'CACHER STATUS  : ' +
				(this.configs.CACHER.ACTIVE
					? chalk.default.greenBright('ACTIVATED')
					: chalk.default.redBright('DEACTIVATED')),
		);
		if (this.configs.CACHER.ACTIVE) {
			console.log(
				'> CACHER CLEANER  : ' +
					(this.configs.CACHER.CLEANER
						? chalk.default.greenBright('ACTIVATED')
						: chalk.default.redBright('DEACTIVATED')),
			);
			if (this.configs.CACHER.CLEANER) {
				console.log(
					'> CLEANER INTERVAL: ' +
						chalk.default.blueBright(this.configs.CACHER.INTERVAL),
				);
				console.log(
					'> REFRESH_TIME    : ' +
						chalk.default.blueBright(
							this.configs.CACHER.REFRESH_TIME,
						),
				);
				console.log(
					'> DYNAMIC CACHER  : ' +
						(this.configs.CACHER.DYNAMIC
							? chalk.default.greenBright('ACTIVATED')
							: chalk.default.redBright('DEACTIVATED')),
				);
			}
		}
		console.log('***************************************');
		if (this.configs.SERVER.DEBUG_LEVEL > 0)
			console.log(
				'*************  ' +
					chalk.default.yellowBright('LOGGING') +
					'  ***************',
			);
	}

	/**
	 * Return debug level
	 */
	get_debug_log() {
		switch (this.configs.SERVER.DEBUG_LEVEL) {
			case 0:
				return chalk.default.blueBright('DISABLED');
			case 1:
				return chalk.default.yellowBright('MINIMUM');
			case 2:
				return chalk.default.yellow('HARD');
			case 3:
				return chalk.default.redBright('MAXIMUM');
		}
	}
}

export { Manager };
