const AppConfig = {
	/**
	 * Name of application
	 */
	APP_NAME: 'MyProject',
	/**
	 * Setting server status
	 */
	SERVER: {
		/**
		 * Status value of server
		 *
		 *		0 Is offline
		 *		1 Is online
		 *		2 Is maintain
		 */
		STATUS: 1,
		/**
		 * Debug level of server
		 *  Max level is 3:
		 *
		 *      Level 0: Disable debug
		 *      Level 1: Debug only failed value
		 *      Level 2: Show success value
		 *      Level 3: Show all value
		 *
		 *  Setting debug level in config file
		 */
		DEBUG_LEVEL: 3,
		/**
		 * Version of server
		 */
		VERSION: 1,
	},
	/**
	 * Token setting for server
	 */
	TOKEN: {
		/**
		 * Name of header that token store in
		 */
		HEADER_KEY: 'Authorization',
		/**
		 * Default token of server that will pass the middleware
		 */
		DEFAULT: 'cdd950de4181b8d19f745781f730b547',
		/**
		 * Require symbol before token
		 */
		SYMBOL: 'Bearer',
		/**
		 * Admin token that can access all api
		 */
		ADMIN: 'C32E97C4E438172F3F2F1ECC7D07B157C294ECF5',
	},
	/**
	 * Setting JWT token of server
	 */
	JWT: {
		/**
		 * Secret jwt key
		 */
		SECRET_KEY: 'g4WiqNbT4d34rjWJhPwzipLKRKdxWg',
		/**
		 * Expire time of jwt token
		 */
		EXPIRE_TIME: 86400,
		/**
		 * Expire time of refresh token
		 */
		REFRESH_TOKEN_EXPIRE_TIME: 604800,
		/**
		 * Header name that token store in
		 */
		HEADER_KEY: 'Authorization',
		/**
		 * Require symbol before token
		 */
		SYMBOL: 'Bearer',
	},
	/**
	 * Setting another url
	 */
	URL: {
		/**
		 * MongoDB url access
		 */
		MONGO_DB: 'mongodb://127.0.0.1/myproject',
		/**
		 * Google authorize token url
		 */
		GOOGLE_AUTHORIZE: '',
	},
	/**
	 * Setting cache of server
	 */
	CACHER: {
		/**
		 * Cache active state
		 */
		ACTIVE: true,
		/**
		 * Should it clean in refresh time
		 */
		CLEANER: true,
		/**
		 * Should it clean base on how many active object
		 */
		DYNAMIC: true,
		/**
		 * Refresh time to clean
		 */
		REFRESH_TIME: 1800000,
		/**
		 * Interval time of cache for checker data
		 */
		INTERVAL: 1800000,
		/**
		 * Config of dynamic cache
		 */
		DYNAMIC_CACHE: 1000,
		/**
		 * Config of dynamic cache
		 */
		DYNAMIC_FREQ_CHECK_USER: 500,
		/**
		 * Config of dynamic cache
		 */
		MAX_DYNAMIC_MULTI: 4,
	},
	/**
	 * Setting optimize ram
	 */
	MEMORY: {
		/**
		 * Should it clean ram on interval time
		 */
		CLEANER: false,
		/**
		 * Setting interval time for cleaning ram
		 */
		INTERVAL: 10000,
	},
	/**
	 * Setting server support language
	 */
	SUPPORT_LANGUAGE: [{ name: 'English', key: 'en' }],
};

export { AppConfig };
