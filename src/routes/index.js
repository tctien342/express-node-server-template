'use strict';
import { Manager } from '../manager';
import { ModelManager } from '../models';
import { JWTAuthorize } from '../etcs/jwt';
import { AppController, ControllerManager } from '../controllers';
import { TokenChecker } from '../middleware';
import { DefaultRouter } from './default';
import { AdminRouter } from './admin';

// INIT INSTANT CONFIG FOR RELOAD-ABLE
let manager = new Manager();

// INIT MODELS
let modelManager = new ModelManager(manager);

// INIT JWT CLASS
let jwtHandler = new JWTAuthorize(manager, modelManager);

// INIT CONTROLLER
let controllers = new ControllerManager(manager, modelManager);

// INIT MIDDLEWARE
let token_mid = new TokenChecker(manager, jwtHandler);

const Routers = {
	DEFAULT: new DefaultRouter(manager, controllers).getRouter(),
	ADMIN: new AdminRouter(manager, controllers).getRouter(),
};

export { Routers };
