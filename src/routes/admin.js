import * as express from 'express';
import {
	responder_type_set,
	checkServer,
	checkAdminToken,
} from '../middleware';
import { Manager } from '../manager';
import { ControllerManager } from '../controllers';

/**
 * Admin router for admin user
 */
class AdminRouter {
	manager: Manager;
	controllers: ControllerManager;
	router: express.Router;

	constructor(manager: Manager, controllers: ControllerManager) {
		this.manager = manager;
		this.controllers = controllers;
		this.router = express.Router();
		// INIT MIDDLEWARE
		this.router.use(responder_type_set);
		this.router.use((req, res, next) =>
			checkAdminToken(req, res, next, manager),
		);
		this.initRoute();
	}

	initRoute() {
		// this.router.post('/check/admin', (req, res) =>
		// 	this.controllers.app_controller.check_admin(req, res),
		// );
	}

	getRouter() {
		return this.router;
	}
}
export { AdminRouter };
