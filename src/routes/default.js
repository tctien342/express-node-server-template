import * as express from 'express';
import { responder_type_set, checkServer, checkUser } from '../middleware';
import { Manager } from '../manager';
import { ControllerManager } from '../controllers';

/**
 * Default router for app
 */
class DefaultRouter {
	manager: Manager;
	controllers: ControllerManager;
	router: express.Router;

	constructor(manager: Manager, controllers: ControllerManager) {
		this.manager = manager;
		this.controllers = controllers;
		this.router = express.Router();

		// INIT MIDDLEWARE
		this.router.use(responder_type_set);
		// this.router.use((req, res, next) =>
		// 	checkServer(req, res, next, this.manager),
		// );
		// this.router.use((req, res, next) =>
		// 	checkUser(req, res, next, this.manager),
		// );
		this.initRoute();
	}

	/**
	 * Init router
	 */
	initRoute() {
		/**
		 * Default test api
		 */
		this.router.get('/get', (req, res) => {
			this.controllers.app_controller.get_default(req, res);
		});
	}

	/**
	 * Get router
	 */
	getRouter() {
		return this.router;
	}
}

export { DefaultRouter };
