import * as express from 'express';
import { JWTAuthorize, Responder } from '../etcs';
import { Manager } from '../manager';

/**
 * Token checker middleware
 */
class TokenChecker {
	manager: Manager;
	jwt: JWTAuthorize;

	constructor(manager: Manager, jwt_obj: JWTAuthorize) {
		this.manager = manager;
		this.jwt = jwt_obj;
	}

	/**
	 * Checking token from request if it valid
	 * @param {express.Request} req request from client
	 * @param {express.Response} res response from server
	 * @param {express.NextFunction} next next function will be progress
	 */
	token_checker(
		req: express.Request,
		res: express.Response,
		next: express.NextFunction,
	) {
		let token_data = req.get(this.manager.configs.JWT.HEADER_KEY);
		if (!token_data) {
			res.send(
				Responder.make({}, Responder.CODE.TOKEN_ERR, 'Token not found'),
			);
		} else {
			let token = token_data.split(' ');
			if (token[0] !== this.manager.configs.JWT.SYMBOL) {
				res.send(
					Responder.make(
						{},
						Responder.CODE.TOKEN_ERR,
						'Token symbol wrong',
					),
				);
			} else {
				this.jwt.verify_token(token[1], status => {
					if (status.status) {
						req.user = status.data;
						next();
					} else {
						if (
							status.data.hasOwnProperty('name') &&
							status.data.name === 'TokenExpiredError'
						) {
							res.send(
								Responder.make(
									status.data,
									Responder.CODE.TOKEN_ERR,
									'Token Expired',
								),
							);
						} else {
							res.send(
								Responder.make(
									status.data,
									Responder.CODE.TOKEN_ERR,
									status.message,
								),
							);
						}
					}
				});
			}
		}
	}
}

export { TokenChecker };
