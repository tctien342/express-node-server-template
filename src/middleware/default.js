import { Responder } from '../etcs';
import { AppConfig } from '../configs';
import { Manager } from '../manager';
import * as express from 'express';

/**
 * Set content type by default
 * @param {express.Request} req request from client
 * @param {express.Response} res response from server
 * @param {express.NextFunction} next next function will be progress
 */
function responder_type_set(
	req: express.Request,
	res: express.Response,
	next: express.NextFunction,
) {
	res.set('Content-Type', 'application/json');
	next();
}
/**
 * Checking if server is online or maintain
 * @param {express.Request} req request from client
 * @param {express.Response} res response from server
 * @param {express.NextFunction} next next function will be progress
 * @param {Manager} manager input manager variable to get global config
 */
function checkServer(
	req: express.Request,
	res: express.Response,
	next: express.NextFunction,
	manager: Manager,
) {
	if (manager.configs.SERVER.STATUS === manager.server_status.NORMAL) {
		next();
	} else if (
		manager.configs.SERVER.STATUS === manager.server_status.OFFLINE
	) {
		res.send(
			Responder.make({}, Responder.CODE.SERVER_ERR, 'Server is offline'),
		);
	} else {
		res.send(
			Responder.make({}, Responder.CODE.SERVER_ERR, 'Server is maintain'),
		);
	}
}

/**
 * Checking if request is from admin
 * @param {express.Request} req request from client
 * @param {express.Response} res response from server
 * @param {express.NextFunction} next next function will be progress
 * @param {Manager} manager input manager variable to get global config
 */
function checkAdminToken(
	req: express.Request,
	res: express.Response,
	next: express.NextFunction,
	manager: Manager,
) {
	if (!req.body.hasOwnProperty('token')) {
		res.send(
			Responder.make(
				{},
				Responder.CODE.INPUT_ERR,
				'Token access not found',
			),
		);
	} else {
		if (req.body.token === manager.configs.TOKEN.ADMIN) {
			next();
		} else {
			res.send(
				Responder.make(
					{},
					Responder.CODE.PERMISSION_ERR,
					'Token not invalid',
				),
			);
		}
	}
}

/**
 * Checking if request is from user by check jwt token
 * @param {express.Request} req request from client
 * @param {express.Response} res response from server
 * @param {express.NextFunction} next next function will be progress
 * @param {Manager} manager input manager variable to get global config
 */
function checkUser(
	req: express.Request,
	res: express.Response,
	next: express.NextFunction,
	manager: Manager,
) {
	let token_data = req.get(manager.configs.TOKEN.HEADER_KEY);
	if (!token_data) {
		res.send(
			Responder.make({}, Responder.CODE.TOKEN_ERR, 'Token not found'),
		);
	} else {
		let token = token_data.split(' ');
		if (token[0] !== manager.configs.TOKEN.SYMBOL) {
			res.send(
				Responder.make({}, Responder.CODE.TOKEN_ERR, 'Wrong token'),
			);
		} else {
			if (token[1] !== manager.configs.TOKEN.DEFAULT) {
				res.send(
					Responder.make(
						{},
						Responder.CODE.TOKEN_ERR,
						'Token is invalid',
					),
				);
			} else next();
		}
	}
}

export { responder_type_set, checkServer, checkUser, checkAdminToken };
