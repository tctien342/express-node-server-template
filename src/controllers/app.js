import { Logger, Responder } from '../etcs';
import { ModelManager } from '../models';
import { Manager } from '../manager';
import * as express from 'express';

/**
 * Default app controller
 */
class AppController {
	manager: Manager;
	logger: Logger;
	models: ModelManager;

	constructor(manager: Manager, models: ModelManager) {
		this.manager = manager;
		this.logger = new Logger('AppController', manager);
		this.models = models;
	}

	/**
	 * Default api of app will return sent data
	 * @param {express.Request} req Request from api
	 * @param {express.Response} res Response to api
	 */
	get_default(req: express.Request, res: express.Response) {
		let data = req.query;
		res.send(Responder.make(data, Responder.CODE.SUCCESS, 'Call success'));
	}
}

export { AppController };
