import { Manager } from '../manager';
import { ModelManager } from '../models';
import { AppController } from './app';

/**
 * Include all controller of app
 */
class ControllerManager {
	manager: Manager;
	modelManager: ModelManager;

	/**
	 * App controller type
	 */
	app_controller: AppController;

	constructor(manager: Manager, modelManager: ModelManager) {
		this.manager = manager;
		this.modelManager = modelManager;
		this.initController();
	}

	/**
	 * Init all controller of app
	 */
	initController() {
		this.app_controller = new AppController(
			this.manager,
			this.modelManager,
		);
	}
}

export { ControllerManager };
