import { Logger } from '../etcs';
import { JWTStore } from './code';
import { Manager } from '../manager';
import * as mongoose from 'mongoose';

/**
 * Models manager class
 */
class ModelManager {
	manager: Manager;
	logger: Logger;

	//Models
	jwt: JWTStore;

	constructor(manager: Manager) {
		this.manager = manager;
		this.logger = new Logger('Models', manager);
		// this.connect();
		// this.init
	}

	/**
	 * Connect to database
	 */
	connect() {
		mongoose.connect(this.manager.configs.URL.MONGO_DB, {
			useNewUrlParser: true,
		});
		let db = mongoose.connection;
		db.on('error', err => {
			this.status = false;
			this.logger.log('connect', err);
		});
		db.once('open', () => {
			this.status = true;
			this.logger.log('connect', 'Success');
		});
	}

	/**
	 * Init all models after connect DB
	 */
	initModels() {
		// this.jwt = new JWTStore(this.manager);
	}
}

export { ModelManager };
