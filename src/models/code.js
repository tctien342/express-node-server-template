import * as mongoose from 'mongoose';
import { Logger, resCall } from '../etcs';
import { Manager } from '../manager';
let Schema = mongoose.Schema;

/**
 * JWT model
 */
class JWTStore {
	model: mongoose.Model;
	schema: mongoose.Schema;
	logger: Logger;
	form = {
		code: '',
		count: 1,
		package: '',
		active: 1,
		start_date: '',
		end_date: '',
	};

	constructor(manager = Manager) {
		this.manager = manager;
		this.schema = JWTStore.code_schema();
		this.model = mongoose.model('JWTStore', this.schema);
		this.logger = new Logger('Code Model', manager);
	}

	/**
	 * JWT schema for mongoDB
	 */
	static code_schema() {
		return new Schema({
			code: { type: String, default: '' },
			count: { type: Number, default: 1 },
			package: { type: Schema.Types.ObjectId, ref: 'Package' },
			active: { type: Number, default: 1 },
			start_date: { type: Date, default: Date.now },
			end_date: { type: Date, required: false },
		});
	}

	/**
	 * Get all object that stored in database
	 * @param {resCall} callback return data from database
	 */
	get_all(callback = () => {}) {
		this.model.find({}, (err, data) => {
			if (err) {
				callback(
					this.logger.result_maker(
						false,
						err,
						'Query all code failed',
						'get_all',
					),
				);
			} else {
				callback(
					this.logger.result_maker(
						true,
						data,
						'Query all code success',
						'get_all',
					),
				);
			}
		});
	}

	/**
	 * Return all active token from database
	 * @param {resCall} callback return all active token
	 */
	get_all_active(callback = () => {}) {
		this.model.find(
			{
				$or: [
					{
						count: { $gt: 0 },
						end_date: { $gt: Date.now() },
						active: 1,
					},
					{ count: { $gt: 0 }, end_date: null, active: 1 },
				],
			},
			(err, data) => {
				if (err) {
					callback(
						this.logger.result_maker(
							false,
							err,
							'Query all active code failed',
							'get_all_active',
						),
					);
				} else {
					callback(
						this.logger.result_maker(
							true,
							data,
							'Query all active code success',
							'get_all_active',
						),
					);
				}
			},
		);
	}

	/**
	 * Get JWT token info from database
	 * @param {string} code jwt token
	 * @param {resCall} callback return jwt token's information
	 */
	get_by_code(code: String, callback = () => {}) {
		this.model
			.findOne({ code: code })
			.populate('package')
			.exec((err, data) => {
				if (err) {
					callback(
						this.logger.result_maker(
							false,
							err,
							'Can not query data ' + code,
							'get_by_code',
						),
					);
				} else {
					if (data) {
						data.count = data.count - 1;
						data.save().then(() => {
							callback(
								this.logger.result_maker(
									true,
									data,
									'Found code ' + code,
									'get_by_code',
								),
							);
						});
					} else {
						callback(
							this.logger.result_maker(
								false,
								{},
								'Package not found ' + code,
								'get_by_code',
							),
						);
					}
				}
			});
	}

	/**
	 * Add code to database
	 * @param {this.form} code_data data of code to be add
	 * @param {resCall} callback return result of progress
	 */
	add_code(code_data = this.form, callback = () => {}) {
		this.get_by_code(code_data.code, state => {
			if (state.status) {
				callback(
					this.logger.result_maker(
						false,
						{},
						'Code is existed ' + code_data.code,
						'add_code',
					),
				);
			} else {
				let new_code = this.model(code_data);
				try {
					new_code.save().then(() => {
						callback(
							this.logger.result_maker(
								true,
								new_code,
								'Add success code: ' + code_data.code,
								'add_code',
							),
						);
					});
				} catch (e) {
					callback(
						this.logger.result_maker(
							false,
							e,
							'Add failed code: ' + code_data.code,
							'add_code',
						),
					);
				}
			}
		});
	}
}
export { JWTStore };
