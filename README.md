# Express template by SaintNo

![Alt text](https://a.icons8.com/nceXjeNw/yGhU0k/group@2x.png)

## Features

    Express NodeJS
    FlowJS and ES6 supported
    Cache feature
    MVC structure
    JWT supported

## Installation

The easiest way to get started is to clone the repository:

```bash
# Get the latest snapshot
git clone --depth=1 https://gitlab.com/tctien342/express-node-server-template.git myproject

# Change directory
cd myproject

# Install NPM dependencies
npm install

# Install gulp globally
npm i gulp -g

# Then simply start your app with the automation tool gulp
gulp
```

Edit your project in src/configs

## Tests

The easiest way to get tests results.

```bash
# Run test with coverage istanbul.

gulp coverage

```
